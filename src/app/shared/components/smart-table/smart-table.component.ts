import {Component, OnInit} from '@angular/core';
import {ServerDataSource} from 'ng2-smart-table';
import {HttpClient} from '@angular/common/http';
import swal from 'sweetalert';

@Component({
  selector: 'app-smart-table',
  styleUrls: ['./smart-table.component.css'],
  template: ' <ng2-smart-table ' +
    '[settings]="settings" ' +
    '[source]="source" ' +
    '(createConfirm)="onCreateConfirm($event)" ' +
    '(editConfirm)="onSaveConfirm($event)"' +
    ' (deleteConfirm)="onDeleteConfirm($event)"></ng2-smart-table>',
})
export class SmartTableComponent implements OnInit {
  settings = {
    pager: {
      display: true,
      perPage: 10, // config page limit
    },
    delete: {
      deleteButtonContent: '<i class="ion ion-md-trash"></i>',
      confirmDelete: true,
    },
    add: {
      addButtonContent: '<i class="ion ion-md-add"></i>',
      createButtonContent: '<i class="ion ion-md-checkmark-circle-outline"></i>',
      cancelButtonContent: '<i class="ion ion-md-close-circle-outline"></i>',
      confirmCreate: true
    },
    edit: {
      editButtonContent: '<i class="fas fa-pencil-alt"></i>',
      saveButtonContent: '<i class="far fa-check-square"></i>',
      cancelButtonContent: '<i class="ion ion-md-close-circle-outline"></i>',
      confirmSave: true
    },

    columns: {
      id: {
        title: 'ID',
        editable: false,
        filter: false
      },
      albumId: {
        title: 'Album',
      },
      title: {
        title: 'Title',
      },
      url: {
        title: 'Url',
      },
    },
  };
  data = [
    {
      albumId: 1,
      id: 2,
      title: 'reprehenderit est deserunt velit ipsam',
      url: 'https://via.placeholder.com/600/771796',
      thumbnailUrl: 'https://via.placeholder.com/150/771796'
    },
    {
      albumId: 1,
      id: 3,
      title: 'officia porro iure quia iusto qui ipsa ut modi',
      url: 'https://via.placeholder.com/600/24f355',
      thumbnailUrl: 'https://via.placeholder.com/150/24f355'
    },
    {
      albumId: 1,
      id: 4,
      title: 'culpa odio esse rerum omnis laboriosam voluptate repudiandae',
      url: 'https://via.placeholder.com/600/d32776',
      thumbnailUrl: 'https://via.placeholder.com/150/d32776'
    },
    {
      albumId: 1,
      id: 5,
      title: 'natus nisi omnis corporis facere molestiae rerum in',
      url: 'https://via.placeholder.com/600/f66b97',
      thumbnailUrl: 'https://via.placeholder.com/150/f66b97'
    },
    {
      albumId: 1,
      id: 6,
      title: 'accusamus ea aliquid et amet sequi nemo',
      url: 'https://via.placeholder.com/600/56a8c2',
      thumbnailUrl: 'https://via.placeholder.com/150/56a8c2'
    },
    {
      albumId: 1,
      id: 7,
      title: 'officia delectus consequatur vero aut veniam explicabo molestias',
      url: 'https://via.placeholder.com/600/b0f7cc',
      thumbnailUrl: 'https://via.placeholder.com/150/b0f7cc'
    },
    {
      albumId: 1,
      id: 8,
      title: 'aut porro officiis laborum odit ea laudantium corporis',
      url: 'https://via.placeholder.com/600/54176f',
      thumbnailUrl: 'https://via.placeholder.com/150/54176f'
    },
    {
      albumId: 1,
      id: 9,
      title: 'qui eius qui autem sed',
      url: 'https://via.placeholder.com/600/51aa97',
      thumbnailUrl: 'https://via.placeholder.com/150/51aa97'
    },
    {
      albumId: 1,
      id: 10,
      title: 'beatae et provident et ut vel',
      url: 'https://via.placeholder.com/600/810b14',
      thumbnailUrl: 'https://via.placeholder.com/150/810b14'
    },
    {
      albumId: 1,
      id: 11,
      title: 'nihil at amet non hic quia qui',
      url: 'https://via.placeholder.com/600/1ee8a4',
      thumbnailUrl: 'https://via.placeholder.com/150/1ee8a4'
    },
    {
      albumId: 1,
      id: 12,
      title: 'mollitia soluta ut rerum eos aliquam consequatur perspiciatis maiores',
      url: 'https://via.placeholder.com/600/66b7d2',
      thumbnailUrl: 'https://via.placeholder.com/150/66b7d2'
    },
    {
      albumId: 1,
      id: 13,
      title: 'repudiandae iusto deleniti rerum',
      url: 'https://via.placeholder.com/600/197d29',
      thumbnailUrl: 'https://via.placeholder.com/150/197d29'
    },
    {
      albumId: 1,
      id: 14,
      title: 'est necessitatibus architecto ut laborum',
      url: 'https://via.placeholder.com/600/61a65',
      thumbnailUrl: 'https://via.placeholder.com/150/61a65'
    },
    {
      albumId: 1,
      id: 15,
      title: 'harum dicta similique quis dolore earum ex qui',
      url: 'https://via.placeholder.com/600/f9cee5',
      thumbnailUrl: 'https://via.placeholder.com/150/f9cee5'
    },
    {
      albumId: 1,
      id: 16,
      title: 'iusto sunt nobis quasi veritatis quas expedita voluptatum deserunt',
      url: 'https://via.placeholder.com/600/fdf73e',
      thumbnailUrl: 'https://via.placeholder.com/150/fdf73e'
    },
    {
      albumId: 1,
      id: 17,
      title: 'natus doloribus necessitatibus ipsa',
      url: 'https://via.placeholder.com/600/9c184f',
      thumbnailUrl: 'https://via.placeholder.com/150/9c184f'
    }
  ];
  source: ServerDataSource;

  constructor(http: HttpClient) {
    this.source = new ServerDataSource(http, {endPoint: 'https://jsonplaceholder.typicode.com/photos'});  // write api here
  }

  // source: LocalDataSource;
  //
  // constructor() {
  //   this.source = new LocalDataSource(this.data);
  // }

  ngOnInit() {
  }

  onCreateConfirm(event) {
    console.log('add Record' + event.newData);
    swal({
      title: 'Are you sure?',
      text: `You want to create?`,
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true
    })
      .then((willCreate) => {
        if (willCreate) {
          event.confirm.resolve();
        } else {
          event.confirm.reject();
        }
      });
  }

  onSaveConfirm(event) {
    console.log('add Record' + event.newData);
    swal({
      title: 'Are you sure?',
      text: `You want to Update!`,
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true
    })
      .then((willEdit) => {
        if (willEdit) {
          event.confirm.resolve();
        } else {
          event.confirm.reject();
        }
      });
  }

  onDeleteConfirm(event) {
    console.log(event.data);
    swal({
      title: 'Are you sure?',
      text: `Once deleted, you will not be able to recover this user information!`,
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true
    })
      .then((willDelete) => {
        if (willDelete) {
          console.log('send api delete');
          event.confirm.resolve();
        } else {
          swal('Your field is safe!');
          event.confirm.reject();
        }
      });
  }
}
