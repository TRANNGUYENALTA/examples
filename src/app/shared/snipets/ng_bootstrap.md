// Cách sử dụng ng bootstrap
url: https://valor-software.com/ngx-bootstrap/?gclid=EAIaIQobChMIl4Gt0v6q3wIVGh4rCh1FFweqEAAYASAAEgK8gfD_BwE#/
<pagination [totalItems]="190" [(ngModel)]="currentPage" [maxSize]="5"></pagination>
<progressbar class="progress-striped active"  [value]="56" type="danger" [max]="300" [animate]="true"><i>136 / 150</i></progressbar>
