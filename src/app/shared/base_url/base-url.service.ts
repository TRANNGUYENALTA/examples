import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseUrlService {
  private BASE_URL = 'http://192.168.11.240';

  getBaseUrl() {
    return this.BASE_URL;
  }
  constructor() {
  }
}
