/*Sử dụng  Attribute Structure
* B1: ClI: ng generate directive nameDirective  || ng g d nameDirect
* B2:  Import và export trong shared module
* B3: Sử dụng  '<div *appUnless="'false'" >Hight Light directives binding from shared module</div>'
* private templatRef: TemplateRef<any>: template sử dụng
*  private vcRef: ViewContainerRef: sử dụng ở đâu
* */
import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {
  @Input() set appUnless(condition: boolean) {
    if (condition) {
      this.vcRef.createEmbeddedView(this.templatRef);
    } else {
      this.vcRef.clear();
    }
  }

  constructor(private templatRef: TemplateRef<any>, private vcRef: ViewContainerRef) {
  }
}
