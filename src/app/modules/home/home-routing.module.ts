import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home.component';
import {LoadingComponent} from '../../shared/components/loading/loading.component';
import {SmartTableComponent} from '../../shared/components/smart-table/smart-table.component';
import {AuthGuard} from '../../cores/guards/auth.guard';

const routes: Routes = [
  {
    path: '', component: HomeComponent, canActivate: [AuthGuard],
    children: [
      {path: 'dashboard', component: SmartTableComponent},
      {path: 'settings', component: LoadingComponent},
      {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
      {path: '**', redirectTo: 'login'},
    ]
  },
  {path: '**', redirectTo: 'login'}

];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class HomeRoutingModule {
}
