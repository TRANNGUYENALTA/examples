import {Component, OnInit} from '@angular/core';
import {HomeService} from '../../home.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private homeService: HomeService,
              private router: Router) {
  }

  onClickEventEmiiter() {
    this.homeService.newEventEmitter.emit('Hello Event Emitter');
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigateByUrl('/login');
  }

  ngOnInit() {
  }

}
