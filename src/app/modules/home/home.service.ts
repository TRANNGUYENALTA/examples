import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class HomeService {
  // create new eventEmitter
  newEventEmitter = new EventEmitter<string>();
  constructor() { }
}
