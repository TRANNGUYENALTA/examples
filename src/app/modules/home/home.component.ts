import {Component, OnInit, OnChanges, OnDestroy} from '@angular/core';
import {HomeService} from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HomeService]
})
export class HomeComponent implements OnInit, OnChanges, OnDestroy {
  public number = Math.random();
  currentPage = 5;
  valueSwitch = 100;

  constructor(private homeService: HomeService) {
    console.log('constructor work');
  }

  ngOnInit() {
    console.log('ngOnInit work');
    this.homeService.newEventEmitter.subscribe(data => {
      alert('Emitter working' + data);
    });

  }
  ngOnChanges() {
    console.log('ngOnChanges work');

  }
  ngOnDestroy() {
    console.log('ngOnDestroy work');

  }

}

