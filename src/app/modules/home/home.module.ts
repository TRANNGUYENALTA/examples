import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {HeaderComponent} from './components/header/header.component';
// import third party
import {ProgressbarModule} from 'ngx-bootstrap';
import {AlertModule} from 'ngx-bootstrap';
import { PaginationModule } from 'ngx-bootstrap/pagination';

@NgModule({
  imports: [
    HomeRoutingModule,
    SharedModule,
    ProgressbarModule.forRoot(),
    AlertModule.forRoot(),
    PaginationModule.forRoot()

  ],
  declarations: [HomeComponent, HeaderComponent]
})
export class HomeModule {
}
