import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {User} from '../../models/user';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  @ViewChild('formLogin') vlForm;
  user: User = new User();
  constructor(private auth: AuthService) {
  }

  ngOnInit() {
    localStorage.removeItem('currentUser');
  }

  onSignIn(f: NgForm) {
    // console.log(f.form.value);
    // console.log(this.vlForm.value);
    this.subscription = this.auth.login(this.vlForm.value);
  }

  resetForm() {
    this.vlForm.reset();
  }

  setForm() {
    this.vlForm.setValue({email: 'superadmin@gmail.com', password: '123456'});
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
