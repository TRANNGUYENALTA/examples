import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseUrlService} from '../../../shared/base_url/base-url.service'; // comment
import {SharedApiService} from '../../../shared/share_service/shared-api.service';
import {Router} from '@angular/router';
import swal from 'sweetalert';


@Injectable({
  providedIn: 'root'
})


export class AuthService implements OnInit {
  private token = '';

  constructor(private http: HttpClient,
              private baseUrl: BaseUrlService,
              private sharedApiService: SharedApiService,
              private router: Router) {
  }

  ngOnInit() {
  }

  getToken() {
    return this.token;
  }

  login(data) {
    return this.sharedApiService.postApi(`/api/user/login`, data).subscribe(res => {
      if (res && res.data) {
        this.token = res.data;
        swal({
          title: 'Good Work',
          text: 'Login Success!',
          icon: 'success',
        });
        localStorage.setItem('currentUser', res.data);
      } else {
        localStorage.setItem('currentUser', '');
      }
      this.router.navigateByUrl('pages/dashboard');
    }, err => {
    });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
  }
}

// Authorization': `Bearer ${AuthService.getToken()
