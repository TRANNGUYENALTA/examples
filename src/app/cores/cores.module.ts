import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  exports: []
})
export class CoresModule {
  constructor(@Optional() @SkipSelf() core: CoresModule) {
    if (core) {
      throw new Error('You shall not run!');
    }
  }
}
